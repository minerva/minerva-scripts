#!/bin/bash

locale-gen en_US.UTF-8
export LC_ALL="en_US.UTF-8"
export LANG="en_US.UTF-8"


#check if db_partition is available
db_disk="/dev/vdb"
if [ -e $db_disk  ]
then
    echo "Disk for database found: $db_disk"
    mounted=`mount |grep "$db_disk" |wc -l`
    if [ "$mounted" = "0" ]
    then
        echo "Disk not mounted. Aborting"
        exit 1
    else
        echo "Partition mounted."
        mount |grep "$db_disk"
    fi
else 
    echo "Partition for database not found: $db_disk"
fi

#install tools required for minerva installation
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y dirmngr apt-transport-https

#install minerva
echo "deb http://repo-r3lab.uni.lu/debian/ stable main" | tee /etc/apt/sources.list.d/repo-r3lab.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0xcb185f4e31872412

echo debconf minerva/internal/skip-preseed boolean false | debconf-set-selections
echo debconf minerva/dbconfig-install boolean true | debconf-set-selections
echo debconf minerva/pgsql/app-pass select 123qweasdzxc | debconf-set-selections
echo debconf minerva/remote/host select localhost | debconf-set-selections
echo debconf minerva/db/dbname select map_viewer | debconf-set-selections
echo debconf minerva/db/app-user select map_viewer@localhost | debconf-set-selections
echo debconf minerva/database-type select pgsql | debconf-set-selections
echo debconf minerva/pgsql/manualconf boolean false | debconf-set-selections


apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y  mc apache2 php libapache2-mod-php minerva

#change tomcat memory limit to 4GB
sed -i 's/-Xmx128m/-Xmx4192m/g' /etc/default/tomcat7
sed -i 's/-Djava.awt.headless=true/-Djava.awt.headless=true -Xmx4192m/g' /etc/default/tomcat8
sed -i 's/-Djava.awt.headless=true/-Djava.awt.headless=true -Xmx4192m/g' /etc/default/tomcat9
service tomcat9 restart

#setup proxy in apache to go to tomcat
sed -i '2i\    ProxyPass /minerva http://localhost:8080/minerva\n    ProxyPass /map_images http://localhost:8080/map_images\n    ProxyPass /minerva-big http://localhost:8080/minerva-big\n    ProxyPassReverse /minerva http://localhost:8080/minerva\n    ProxyPassReverse /minerva-big http://localhost:8080/minerva-big\n    ProxyPassReverse /map_images http://localhost:8080/map_images\n' /etc/apache2/sites-enabled/000-default.conf

a2enmod proxy
a2enmod proxy_http
service apache2 restart

#add redirection
printf "<?php\nheader('Location: minerva/');\n?>\n" > /var/www/html/index.php
rm /var/www/html/index.html

#create SSL certificat
sudo mkdir /etc/apache2/ssl
sudo openssl genrsa -des3 -passout pass:xxxxyyyy -out /etc/apache2/ssl/server.pass.key 2048
sudo openssl rsa -passin pass:xxxxyyyy -in /etc/apache2/ssl/server.pass.key -out /etc/apache2/ssl/server.key
sudo rm /etc/apache2/ssl/server.pass.key
sudo openssl req -new -key /etc/apache2/ssl/server.key -out /etc/apache2/ssl/server.csr   -subj "/C=LU/ST=Luxembourg/L=Esch-sur-Alzette/O=UL/OU=LCSB/CN=example.com"
sudo openssl x509 -req -days 365 -in /etc/apache2/ssl/server.csr -signkey /etc/apache2/ssl/server.key -out /etc/apache2/ssl/server.crt

#set the SSL certificate in apache2
sudo sed -i.bak -- 's/\/etc\/ssl\/certs\/ssl-cert-snakeoil\.pem/\/etc\/apache2\/ssl\/server.crt/g' /etc/apache2/sites-available/default-ssl.conf
sudo sed -i.bak -- 's/\/etc\/ssl\/private\/ssl-cert-snakeoil\.key/\/etc\/apache2\/ssl\/server.key/g' /etc/apache2/sites-available/default-ssl.conf
sudo a2ensite default-ssl
sudo a2enmod ssl

sudo sed -i.bak '/ProxyPass/d' /etc/apache2/sites-enabled/default-ssl.conf
sudo sed -i '3i\    ProxyPass / http://localhost/\n    ProxyPassReverse / http://localhost/\n' /etc/apache2/sites-enabled/default-ssl.conf

#log IPs behind reverse proxy
echo >>/etc/apache2/apache2.conf
echo "RemoteIPHeader X-Forwarded-For" >> /etc/apache2/apache2.conf
echo "RemoteIPTrustedProxy 10.244.9.39 10.244.9.34" >> /etc/apache2/apache2.conf
echo "RemoteIPInternalProxy 10.244.9.39 10.244.9.34" >> /etc/apache2/apache2.conf
echo 'LogFormat "%a %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined' >> /etc/apache2/apache2.conf
a2enmod remoteip

sudo service apache2 restart

#turn on backups
ln minerva-scripts/server/backup.sh /etc/cron.daily/minerva

#create .pgpass for later db management
echo "localhost:5432:map_viewer:map_viewer:123qweasdzxc" > .pgpass
echo "127.0.0.1:5432:map_viewer:map_viewer:123qweasdzxc" >> .pgpass
chmod 0600 .pgpass
user_name=`env | grep SUDO_USER |cut -f2 -d'='`
if [ "$user_name" = "" ]
then
	user_name=`whoami`
fi
echo "USER: $user_name"
chown $user_name:$user_name .pgpass

#define default hostname and user for 'psql' command
echo >>.bashrc
echo "export PGUSER=map_viewer" >>.bashrc
echo "export PGHOST=127.0.0.1" >>.bashrc

#wait for tomcat to start
sleep 10
curl localhost:8080/minerva/api/configuration/ -o /dev/null
