#!/bin/bash
BACKUP_FILE="/opt/backup/backup.tar.gz"
BACKUP_DIR="/tmp/backup/"
BACKUP_USER="root"

rm -f $BACKUP_FILE
rm -rdf $BACKUP_DIR
mkdir -p $BACKUP_DIR/tmp
mkdir -p "${BACKUP_FILE%/*}"

tar -zcf $BACKUP_DIR/tmp/files.tar.gz -C / usr/share/minerva

export PGPASSWORD="123qweasdzxc"
pg_dump -U map_viewer map_viewer -h localhost | gzip > "$BACKUP_DIR/tmp/map_viewer_db.gz"

cd $BACKUP_DIR/tmp
tar -zcf $BACKUP_FILE *

#change owner of backup file only if user exists
id -u $BACKUP_USER >/dev/null
status=$?
[ $status -eq 0 ] && chown $BACKUP_USER $BACKUP_FILE

cd /
rm -rf $BACKUP_DIR/tmp

