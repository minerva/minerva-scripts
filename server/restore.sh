#!/bin/bash

sudo locale-gen en_US.UTF-8
export LC_ALL="en_US.UTF-8"
export LANG="en_US.UTF-8"

sudo service tomcat9 stop
sudo cp /opt/backup/backup.tar.gz .
tar -zxf backup.tar.gz
sudo rm backup.tar.gz

rm minerva.war

if [ -e "map_images.tar.gz" ]
then
    if [ -e "/var/lib/tomcat7" ]
    then
        CATALINA_HOME="/var/lib/tomcat7"
    fi

    if [ -e "/var/lib/tomcat8" ]
    then
        CATALINA_HOME="/var/lib/tomcat8"
    fi
    if [ -e "/var/lib/tomcat9" ]
    then
        CATALINA_HOME="/var/lib/tomcat9"
    fi
    sudo rm -rf $CATALINA_HOME/webapps/map_images/

    tar -zxf map_images.tar.gz
    sudo mv map_images $CATALINA_HOME/webapps/
    sudo chown -R tomcat:tomcat $CATALINA_HOME/webapps/map_images/
    sudo chown -R tomcat7:tomcat7 $CATALINA_HOME/webapps/map_images/
    sudo chown -R tomcat8:tomcat8 $CATALINA_HOME/webapps/map_images/
    sudo chown -R tomcat9:tomcat9 $CATALINA_HOME/webapps/map_images/
fi
rm map_images.tar.gz

if [ ! -e "~/.pgpass" ]
then
    echo "*:5432:*:map_viewer:123qweasdzxc" >~/.pgpass
    chmod 0600 ~/.pgpass
fi

gzip -d map_viewer_db.gz
sudo sudo -u postgres dropdb map_viewer
sudo sudo -u postgres createdb map_viewer -O map_viewer
psql -h localhost -U map_viewer -f map_viewer_db map_viewer


rm map_viewer_db

sudo service tomcat9 start
