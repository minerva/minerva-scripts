#!/bin/bash

db_disk="/dev/vdb"
db_location="/var/lib/postgresql"

#check if disk is available
if [ -e $db_disk  ]
then
    mounted=`mount |grep "$db_disk" |wc -l`
    if [ "$mounted" = "0" ]
    then
        file_system_ok=`sudo fsck -N /dev/vdb |grep ext4 |wc -l`

        if [ "$file_system_ok" = "0" ]
        then
           echo "Initializing file system"
           sudo mkfs.ext4 $db_disk
           echo "Mounting file system"
           sudo mkdir -p $db_location
           sudo echo "$db_disk $db_location    ext4    defaults    0   0">>/etc/fstab
           sudo mount $db_disk

        else
            echo "File system already exists. Aborting"
            exit 1
        fi
    else
        echo "Partition mounted. Skipping"
        mount |grep "$db_disk"
    fi

else 
    echo "Disk for database not found: $db_disk"
fi

exit

