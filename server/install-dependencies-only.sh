#!/bin/bash

#check if db_partition is available
db_disk="/dev/vdb"
if [ -e $db_disk  ]
then
    echo "Disk for database found: $db_disk"
    mounted=`mount |grep "$db_disk" |wc -l`
    if [ "$mounted" = "0" ]
    then
        echo "Disk not mounted. Aborting"
        exit 1
    else
        echo "Partition mounted."
        mount |grep "$db_disk"
    fi
else 
    echo "Partition for database not found: $db_disk"
fi

#install toole required for minerva installation
apt-get update && apt-get install dirmngr apt-transport-https

#install minerva
echo "deb http://repo-r3lab.uni.lu/debian/ stable main" | tee /etc/apt/sources.list.d/repo-r3lab.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0xcb185f4e31872412
apt-get update
apt-get install -y  mc apache2 php libapache2-mod-php7.0 tomcat8 postgresql

#change tomcat memory limit to 4GB
sed -i 's/-Djava.awt.headless=true/-Djava.awt.headless=true -Xmx4192m/g' /etc/default/tomcat8

#setup proxy in apache to go to tomcat
sed -i '2i\    ProxyPass /minerva http://localhost:8080/minerva\n    ProxyPass /map_images http://localhost:8080/map_images\n    ProxyPassReverse /minerva http://localhost:8080/minerva\n    ProxyPassReverse /map_images http://localhost:8080/map_images\n' /etc/apache2/sites-enabled/000-default.conf

a2enmod proxy
a2enmod proxy_http
service apache2 restart

#add redirection
printf "<?php\nheader('Location: minerva/');\n?>\n" > /var/www/html/index.php
rm /var/www/html/index.html

#create SSL certificat
sudo mkdir /etc/apache2/ssl
sudo openssl genrsa -des3 -passout pass:xxxxyyyy -out /etc/apache2/ssl/server.pass.key 2048
sudo openssl rsa -passin pass:xxxxyyyy -in /etc/apache2/ssl/server.pass.key -out /etc/apache2/ssl/server.key
sudo rm /etc/apache2/ssl/server.pass.key
sudo openssl req -new -key /etc/apache2/ssl/server.key -out /etc/apache2/ssl/server.csr   -subj "/C=LU/ST=Luxembourg/L=Esch-sur-Alzette/O=UL/OU=LCSB/CN=example.com"
sudo openssl x509 -req -days 365 -in /etc/apache2/ssl/server.csr -signkey /etc/apache2/ssl/server.key -out /etc/apache2/ssl/server.crt

#set the SSL certificate in apache2
sudo sed -i.bak -- 's/\/etc\/ssl\/certs\/ssl-cert-snakeoil\.pem/\/etc\/apache2\/ssl\/server.crt/g' /etc/apache2/sites-available/default-ssl.conf
sudo sed -i.bak -- 's/\/etc\/ssl\/private\/ssl-cert-snakeoil\.key/\/etc\/apache2\/ssl\/server.key/g' /etc/apache2/sites-available/default-ssl.conf
sudo a2ensite default-ssl
sudo a2enmod ssl

sudo sed -i.bak '/ProxyPass/d' /etc/apache2/sites-enabled/default-ssl.conf
sudo sed -i '3i\    ProxyPass / http://localhost/\n    ProxyPassReverse / http://localhost/\n' /etc/apache2/sites-enabled/default-ssl.conf

sudo service apache2 restart

#create database and db user
su - postgres -c "createuser -d -r -s map_viewer"
su - postgres -c "echo \"ALTER USER map_viewer WITH PASSWORD '123qweasdzxc';\"| psql"
su - postgres -c "createdb -O map_viewer map_viewer"

hba_conf=`su - postgres -c "psql -t -P format=unaligned -c 'show hba_file';"`;
cp $hba_conf $hba_conf".bac"
cat $hba_conf".bac" |grep -v "all[ \t]*peer" >$hba_conf
printf "local   all             all                                     md5\n" >>$hba_conf
service postgresql restart

#turn on backups
ln minerva-scripts/server/backup.sh /etc/cron.daily/minerva
