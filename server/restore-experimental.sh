#!/bin/bash

sudo service tomcat8 stop
sudo cp /opt/backup/backup.tar.gz .
tar -zxf backup.tar.gz

gzip -d map_viewer_db.gz

if [ -e "/var/lib/tomcat7" ]
then
    CATALINA_HOME="/var/lib/tomcat7"
fi

if [ -e "/var/lib/tomcat8" ]
then
    CATALINA_HOME="/var/lib/tomcat8"
fi

if [ -e "map_images.tar.gz" ]
then
    tar -zxf map_images.tar.gz

    sudo rm -rf $CATALINA_HOME/webapps/map_images/

    sudo mv map_images $CATALINA_HOME/webapps/
fi

if [ ! -e "~/.pgpass" ]
then
    echo "localhost:5432:*:map_viewer:123qweasdzxc" >~/.pgpass
    chmod 0600 ~/.pgpass
fi

dropdb -U map_viewer map_viewer; 
createdb -U map_viewer map_viewer; 
psql -U map_viewer -f map_viewer_db map_viewer

if [ -e "$CATALINA_HOME/webapps/minerva.war" ]
then
    sudo rm $CATALINA_HOME/webapps/minerva.war
    sudo rm -rf $CATALINA_HOME/webapps/minerva/
fi
sudo mv minerva.war $CATALINA_HOME/webapps/

sudo rm backup.tar.gz
rm map_images.tar.gz
rm map_viewer_db

sudo service tomcat8 start
