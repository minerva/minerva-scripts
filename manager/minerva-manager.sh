#!/bin/bash

function restore_minerva() {
	host=$1
	directory=$2
	if [ "$host" = "" ]
	then
		echo "Server not defined"
		exit 2
	fi
	if [ "$directory" = "" ]
	then
		directory="~/backup/$host/"
	fi
	echo "Uploading backup $directory/backup.tar.gz"
	echo "put -r $directory/backup.tar.gz" | sftp -b - $host
	echo "Restoring from backup"
	ssh $host "sudo mkdir -p /opt/backup/"
	ssh $host "sudo mv backup.tar.gz /opt/backup/"
	ssh $host "./minerva-scripts/server/restore.sh"
	
}

function backup_minerva() {
	host=$1
	directory=$2
	if [ "$host" = "" ]
	then
		echo "Server not defined"
		exit 2
	fi
	if [ "$directory" = "" ]
	then
		directory="~/backup/$host/"
	fi
	mkdir -p $directory
	echo "Creating backup"
	ssh $host "sudo ./minerva-scripts/server/backup.sh"
	ssh $host "sudo mv /opt/backup/backup.tar.gz ."
	echo "Downloading backup into $directory"
	
	echo "get -r backup.tar.gz $directory" | sftp -b - $host
}

function install_minerva() {
	host=$1
	if [ "$host" = "" ]
	then
		echo "Server not defined"
		exit 2
	fi
	ssh $host "sudo apt-get update; sudo apt-get install -y sudo git"
	ssh $host "rm -rf minerva-scripts"
	ssh $host "git clone https://git-r3lab.uni.lu/piotr.gawron/minerva-scripts.git"
	ssh $host "sudo ./minerva-scripts/server/create-partition.sh"
	ssh $host "sudo ./minerva-scripts/server/install.sh"

	ssh $host "psql -h localhost -U map_viewer -c \"update configuration_option_table set value = 'lcsb-cdc-lums-01.uni.lu' where type='LDAP_ADDRESS';\""
	ssh $host "psql -h localhost -U map_viewer -c \"update configuration_option_table set value = '636' where type='LDAP_PORT';\""
	ssh $host "psql -h localhost -U map_viewer -c \"update configuration_option_table set value = 'true' where type='LDAP_SSL';\""
	ssh $host "psql -h localhost -U map_viewer -c \"update configuration_option_table set value = 'uid=ldap-reader-minerva,cn=users,cn=accounts,dc=uni,dc=lu' where type='LDAP_BIND_DN';\""
	ssh $host "psql -h localhost -U map_viewer -c \"update configuration_option_table set value = 'cn=users,cn=accounts,dc=uni,dc=lu' where type='LDAP_BASE_DN';\""
}

function install_experimental_minerva() {
	host=$1
	directory=$2
	if [ "$host" = "" ]
	then
		echo "Server not defined"
		exit 2
	fi
	if [ "$directory" = "" ]
	then
		directory="~/backup/$host/"
	fi
	echo "Uploading backup $directory/backup.tar.gz"
	echo "put -r $directory/backup.tar.gz" | sftp -b - $host
	ssh $host "sudo mv backup.tar.gz /opt/backup/"
	ssh $host "sudo apt-get update; sudo apt-get install -y sudo git"
	ssh $host "git clone https://git-r3lab.uni.lu/piotr.gawron/minerva-scripts.git"
	ssh $host "sudo ./minerva-scripts/server/create-partition.sh"
	ssh $host "sudo ./minerva-scripts/server/install-dependencies-only.sh"
	ssh $host "sudo ./minerva-scripts/server/restore-experimental.sh"
}

if [ "$1" = "install" ] 
then
	install_minerva "$2"
elif [ "$1" = "backup" ]
then
	backup_minerva "$2" "$3"
elif [ "$1" = "restore" ]
then
	restore_minerva "$2" "$3"
elif [ "$1" = "install-experimental" ]
then
	install_experimental_minerva "$2" "$3"
else
	echo "Unknown command: $1"
	echo Available commands:
	echo - install HOST_NAME
	echo - backup HOST_NAME DIRECTORY
	echo - restore HOST_NAME DIRECTORY
	exit 1
fi
